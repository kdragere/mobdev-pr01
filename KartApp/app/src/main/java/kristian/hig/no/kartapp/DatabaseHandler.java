package kristian.hig.no.kartapp;

/**
 * Created by Kristian on 12.09.2015.
 * SOURCE http://www.javatpoint.com/android-sqlite-tutorial
 */

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "positions";
    private static final String TABLE_LAST_POS = "lastPosition";
    private static final String KEY_ID = "id";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LON = "lon";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_POS_TABLE = "CREATE TABLE " + TABLE_LAST_POS  + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_LAT + " TEXT,"
                + KEY_LON + " TEXT" + ")";
        db.execSQL(CREATE_POS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LAST_POS );

        // Create tables again
        onCreate(db);
    }

    // code to add the new position
    void addPosition(DatabasePosition databasePosition) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LAT, databasePosition.getLat());
        values.put(KEY_LON, databasePosition.getLon());

        // Inserting Row
        db.insert(TABLE_LAST_POS , null, values);
        db.close(); // Closing database connection
    }

    // code to get a single position
    DatabasePosition getDatabasePosition(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LAST_POS , new String[] { KEY_ID,
                        KEY_LAT, KEY_LON }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        DatabasePosition databasePosition = new DatabasePosition(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        return databasePosition;
    }

    // code to get all positions in a list view
    public List<DatabasePosition> getAllPositions() {
        List<DatabasePosition> contactList = new ArrayList<DatabasePosition>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LAST_POS ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabasePosition databasePosition = new DatabasePosition();
                databasePosition.setID(Integer.parseInt(cursor.getString(0)));
                databasePosition.setLat(cursor.getString(1));
                databasePosition.setLon(cursor.getString(2));
                // Adding pos to list
                contactList.add(databasePosition);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
}