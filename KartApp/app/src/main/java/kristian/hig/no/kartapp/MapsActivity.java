package kristian.hig.no.kartapp;

import org.json.JSONArray;
import org.json.JSONException;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity {
    private GoogleMap mMap;
    DatabaseHandler db;
    GPSTracker gps;
    UiSettings mapSettings;


    double latitude;
    double longitude;
    double altitude;
    String strLastLatitude = null;
    String strLastLongitude = null;
    String locationName;
    Integer posId;
    String temperature;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Loop throug all last positions from DB
        // Last position-data will be loaded into variables.
        db = new DatabaseHandler(this); //DB
        List<DatabasePosition> databasePositions = db.getAllPositions();
        for (DatabasePosition pos : databasePositions) {
            if (!databasePositions.isEmpty()) {
                posId = pos.getID();
                strLastLatitude = pos.getLat();
                strLastLongitude = pos.getLon();
            }
        }

        // Set up first acitity with map view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        Intent intent = getIntent();
        mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true);
        getAddress();

        // Creating new thread to get weather
        // It only works multithreaded
        new Thread(new Runnable() {
            public void run(){
                temperature = weatherForecast(latitude, longitude);
            }
        }).start();



    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
             if (mMap != null) {
               setUpMap();
            }
        }
    }

    public void setUpMap() {
        Button getPos = (Button) findViewById(R.id.getPos);
        gps = new GPSTracker(MapsActivity.this);

        //Find position data
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            altitude = gps.getAltitude();

            // Add new marker i map with previous position
            // Converting strings from DB to double
            if (strLastLatitude != null && strLastLongitude != null) {
                double lastLat = Double.parseDouble(strLastLatitude);
                double lastLon = Double.parseDouble(strLastLongitude);
                LatLng lastPos = new LatLng(lastLat, lastLon);
                mMap.addMarker(new MarkerOptions().position(lastPos).title("Last Position"));
            }

            //Convert current positions data to string and write to DB
            strLastLatitude = Double.toString(latitude);
            strLastLongitude = Double.toString(longitude);
            db.addPosition(new DatabasePosition(strLastLatitude, strLastLongitude));
        }
    }
    // This is initalized with a button
    // Adds a new marker to current position and zoom in
    public void findMe(final View v) {
        int zoom = 10;
        LatLng current = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(current).title("Current Position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, zoom));
    }

    // Initalized with a button
    // Creates output data to second acivity + transfer data to activity
    public void positionDescription(final View v) {
        DecimalFormat formatter = new DecimalFormat("#0");  // Format for altitude
        String currentPos = locationName + "currently "+formatter.format(altitude)+" meters above sea level, and the local temperature is "+temperature+" degrees Celcius.";
        Button descr = (Button) findViewById(R.id.descr);
        final Intent i = new Intent(this, positionDescription.class);
        i.putExtra("var", currentPos);
        startActivity(i);
    }


    // SOURCE http://stackoverflow.com/questions/9409195/how-to-get-complete-address-from-latitude-and-longitude
    //Find the address to current positions
    public void getAddress() {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String city = addresses.get(0).getLocality();
            String address = addresses.get(0).getAddressLine(0);
            locationName = "You are in "+city+" ("+address+"), ";

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    // Worked with this function in coorperation with Per-Kristian Nilsen (13HBPUA)
    //Used in weatherForecast function
    public String readStream(InputStream inputStream) throws IOException {
        Reader reader;
        reader = new InputStreamReader(inputStream);
        char[] temp = new char[800];
        reader.read(temp);
        return new String(temp);
    }

    // Worked with this function in coorperation with Per-Kristian Nilsen (13HBPUA)
    // Parse JASON data from URL and output degrees
    public String weatherForecast(double lat, double lng){
        InputStream inputStream;
        try{
            String degrees;
            String URL = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lng;
            URL weatherUrl = new URL(URL);
            HttpURLConnection connection = (HttpURLConnection) weatherUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            inputStream = connection.getInputStream();
            String jsonAsString = readStream(inputStream);

            if (inputStream != null){
                inputStream.close();
            }

            JSONObject result = new JSONObject(jsonAsString);
            JSONArray weatherArray = new JSONArray();
            weatherArray.put(result.getJSONArray("weather"));
            JSONObject mainObject = result.getJSONObject("main");

            double kelvin = mainObject.getDouble("temp");
            double celsius = (kelvin - 273.15);
            int celsiusRounded = (int) Math.round(celsius);
            degrees = String.valueOf(celsiusRounded);

            return degrees;

        }catch(MalformedURLException mal){
            mal.printStackTrace();
        }
        catch(IOException io){
            io.printStackTrace();
        }
        catch(JSONException j){
            j.printStackTrace();
        }
        finally {

        }
        return null;
    }

}

