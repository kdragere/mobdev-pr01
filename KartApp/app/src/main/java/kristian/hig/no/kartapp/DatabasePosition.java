package kristian.hig.no.kartapp;

/**
 * Created by Kristian on 12.09.2015.
 * SOURCE : http://www.javatpoint.com/android-sqlite-tutorial
 * This class helps transfer of data between MapsActivity and DatabaseHandler
 */

public class DatabasePosition {
    int _id;
    String lat;
    String lon;
    public DatabasePosition(){   }
    public DatabasePosition(int id, String lat, String lon){
        this._id = id;
        this.lat = lat;
        this.lon = lon;
    }

    public DatabasePosition(String lat, String lon){
        this.lat = lat;
        this.lon = lon;
    }
    public int getID(){
        return this._id;
    }

    public void setID(int id){
        this._id = id;
    }

    public String getLat(){
        return this.lat;
    }

    public void setLat(String lat){
        this.lat = lat;
    }

    public String getLon(){
        return this.lon;
    }

    public void setLon(String lon){
        this.lon = lon;
    }
}
